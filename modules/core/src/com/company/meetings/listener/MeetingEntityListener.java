package com.company.meetings.listener;

import org.springframework.stereotype.Component;
import com.haulmont.cuba.core.listener.BeforeInsertEntityListener;
import com.haulmont.cuba.core.EntityManager;
import com.company.meetings.entity.Meeting;
import com.haulmont.cuba.core.listener.BeforeDeleteEntityListener;
import com.haulmont.cuba.core.listener.BeforeUpdateEntityListener;

@Component("meetings_MeetingEntityListener")
public class MeetingEntityListener implements BeforeInsertEntityListener<Meeting>, BeforeDeleteEntityListener<Meeting>, BeforeUpdateEntityListener<Meeting> {


    @Override
    public void onBeforeInsert(Meeting entity, EntityManager entityManager) {
        
    }

    
    @Override
    public void onBeforeDelete(Meeting entity, EntityManager entityManager) {

    }


    @Override
    public void onBeforeUpdate(Meeting entity, EntityManager entityManager) {

    }


}