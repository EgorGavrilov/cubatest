-- begin MEETINGS_MEETING
create table MEETINGS_MEETING (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    NAME varchar(255) not null,
    DATE_TIME timestamp not null,
    PLACE varchar(255),
    NOTE varchar(255),
    SECRETARY_ID varchar(36),
    PRESIDENT_ID varchar(36),
    --
    primary key (ID)
)^
-- end MEETINGS_MEETING
-- begin MEETINGS_AGENDA
create table MEETINGS_AGENDA (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    NAME varchar(255) not null,
    FILE_ID varchar(36),
    MEETING_ID varchar(36),
    SUBJECT varchar(255),
    --
    primary key (ID)
)^
-- end MEETINGS_AGENDA
-- begin MEETINGS_EMPLOYEE
create table MEETINGS_EMPLOYEE (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    EMAIL varchar(255),
    PHONE varchar(20),
    NAME varchar(255),
    LASTNAME varchar(255),
    --
    primary key (ID)
)^
-- end MEETINGS_EMPLOYEE
