-- begin MEETINGS_AGENDA
alter table MEETINGS_AGENDA add constraint FK_MEETINGS_AGENDA_ON_FILE foreign key (FILE_ID) references SYS_FILE(ID)^
alter table MEETINGS_AGENDA add constraint FK_MEETINGS_AGENDA_ON_MEETING foreign key (MEETING_ID) references MEETINGS_MEETING(ID)^
create index IDX_MEETINGS_AGENDA_ON_FILE on MEETINGS_AGENDA (FILE_ID)^
create index IDX_MEETINGS_AGENDA_ON_MEETING on MEETINGS_AGENDA (MEETING_ID)^
-- end MEETINGS_AGENDA
-- begin MEETINGS_MEETING
alter table MEETINGS_MEETING add constraint FK_MEETINGS_MEETING_ON_SECRETARY foreign key (SECRETARY_ID) references MEETINGS_EMPLOYEE(ID)^
alter table MEETINGS_MEETING add constraint FK_MEETINGS_MEETING_ON_PRESIDENT foreign key (PRESIDENT_ID) references MEETINGS_EMPLOYEE(ID)^
create index IDX_MEETINGS_MEETING_ON_SECRETARY on MEETINGS_MEETING (SECRETARY_ID)^
create index IDX_MEETINGS_MEETING_ON_PRESIDENT on MEETINGS_MEETING (PRESIDENT_ID)^
-- end MEETINGS_MEETING
