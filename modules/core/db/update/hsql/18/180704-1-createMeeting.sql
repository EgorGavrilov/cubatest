create table MEETINGS_MEETING (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    NAME varchar(255) not null,
    DATE_TIME timestamp not null,
    PLACE varchar(255),
    SECRETARY_ID varchar(36),
    PRESIDENT_ID varchar(36),
    NOTE varchar(255),
    --
    primary key (ID)
);
