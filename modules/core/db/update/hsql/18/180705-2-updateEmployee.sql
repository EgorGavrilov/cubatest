alter table MEETINGS_EMPLOYEE alter column USER_ID rename to USER_ID__U87959 ^
drop index IDX_MEETINGS_EMPLOYEE_ON_USER ;
alter table MEETINGS_EMPLOYEE drop constraint FK_MEETINGS_EMPLOYEE_ON_USER ;
alter table MEETINGS_EMPLOYEE add column EMAIL varchar(255) ;
alter table MEETINGS_EMPLOYEE add column PHONE varchar(20) ;
alter table MEETINGS_EMPLOYEE add column NAME varchar(255) ;
alter table MEETINGS_EMPLOYEE add column LASTNAME varchar(255) ;
