alter table MEETINGS_MEETING alter column PRESIDENT_ID rename to PRESIDENT_ID__U42143 ^
drop index IDX_MEETINGS_MEETING_ON_PRESIDENT ;
alter table MEETINGS_MEETING drop constraint FK_MEETINGS_MEETING_ON_PRESIDENT ;
alter table MEETINGS_MEETING alter column SECRETARY_ID rename to SECRETARY_ID__U65877 ^
drop index IDX_MEETINGS_MEETING_ON_SECRETARY ;
alter table MEETINGS_MEETING drop constraint FK_MEETINGS_MEETING_ON_SECRETARY ;
alter table MEETINGS_MEETING add column SECRETARY_ID varchar(36) ;
alter table MEETINGS_MEETING add column PRESIDENT_ID varchar(36) ;
