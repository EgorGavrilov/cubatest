alter table MEETINGS_AGENDA add constraint FK_MEETINGS_AGENDA_ON_FILE foreign key (FILE_ID) references SYS_FILE(ID);
create index IDX_MEETINGS_AGENDA_ON_FILE on MEETINGS_AGENDA (FILE_ID);
