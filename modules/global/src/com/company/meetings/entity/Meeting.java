package com.company.meetings.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import com.haulmont.chile.core.annotations.Composition;
import com.haulmont.cuba.core.entity.annotation.OnDelete;
import com.haulmont.cuba.core.global.DeletePolicy;
import com.haulmont.cuba.security.entity.User;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.annotation.Listeners;
import com.haulmont.chile.core.annotations.MetaProperty;
import javax.persistence.Transient;
import java.util.Set;
import com.haulmont.cuba.core.entity.annotation.Lookup;
import com.haulmont.cuba.core.entity.annotation.LookupType;
import javax.persistence.ManyToOne;

@Listeners("meetings_MeetingEntityListener")
@NamePattern("%s|name")
@Table(name = "MEETINGS_MEETING")
@Entity(name = "meetings$Meeting")
public class Meeting extends StandardEntity {
    private static final long serialVersionUID = 6664784828067831313L;

    @NotNull
    @Column(name = "NAME", nullable = false)
    protected String name;

    @Temporal(TemporalType.TIMESTAMP)
    @NotNull
    @Column(name = "DATE_TIME", nullable = false)
    protected Date dateTime;

    @Column(name = "PLACE")
    protected String place;

    @Column(name = "NOTE")
    protected String note;

    @Lookup(type = LookupType.DROPDOWN)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SECRETARY_ID")
    protected Employee secretary;

    @Lookup(type = LookupType.DROPDOWN)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PRESIDENT_ID")
    protected Employee president;

    public Employee getSecretary() {
        return secretary;
    }

    public void setSecretary(Employee secretary) {
        this.secretary = secretary;
    }


    public Employee getPresident() {
        return president;
    }

    public void setPresident(Employee president) {
        this.president = president;
    }






    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getPlace() {
        return place;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getNote() {
        return note;
    }


}