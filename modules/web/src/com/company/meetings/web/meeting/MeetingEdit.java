package com.company.meetings.web.meeting;

import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.gui.app.security.user.browse.UserBrowser;
import com.haulmont.cuba.gui.app.security.user.edit.UserEditor;
import com.haulmont.cuba.gui.components.AbstractEditor;
import com.company.meetings.entity.Meeting;
import com.haulmont.cuba.gui.data.CollectionDatasource;
import com.haulmont.cuba.gui.data.GroupDatasource;
import com.haulmont.cuba.security.entity.User;

import javax.inject.Inject;
import javax.xml.crypto.Data;
import java.util.Collection;
import java.util.Collections;
import java.util.UUID;
import com.haulmont.cuba.gui.components.Component;

public class MeetingEdit extends AbstractEditor<Meeting> {
    
    public void onMessage(Component source) {
        showMessageDialog("darova", "kak dela?", MessageType.WARNING);
    }
}